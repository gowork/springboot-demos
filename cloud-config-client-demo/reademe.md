### 访问configserver中的配置文件步骤
- pom.xml中引入
    ```xml
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-config</artifactId>
    </dependency>
    ```
- resources目录下加入bootstrap.properties
    > 注意加入访问配置中心的文件一定要放在bootstrap.properties中，因为该文件会在application.properties文件之前加载，优先级高于默认配置文件
    ```
    #与配置文件中的{application}部分
    spring.application.name=configclient
    #对应配置文件中的{profile}部分
    spring.cloud.config.profile=dev
    #对应git相应的分支
    spring.cloud.config.label=master
    #配置中心的地址
    spring.cloud.config.uri=http://localhost:7001/
    ```
    
- 使用
    
    然后就可以在类中像配置文件配置在当前微服务一样应用
 