package com.stooges.service;

import com.stooges.domain.Person;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 张超
 * @date 2017/5/9
 */
@Service
public class PersonService {
    @Autowired
    private SqlSession sqlSession;
    public Person getPersonById(Long id){
        return sqlSession.selectOne("com.stooges.mapper.PersonMapper.getPersonById",id);
    }
}
