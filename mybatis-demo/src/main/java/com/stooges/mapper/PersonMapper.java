package com.stooges.mapper;

import com.stooges.domain.Person;
import org.apache.ibatis.annotations.Param;

/**
 * @author 张超
 * @date 2017/5/10
 */
//@Mapper
public interface PersonMapper {
//    @Select("select id,name,age from t_person where id=#{id}")
    Person getPersonById(@Param("id") Long id);
}
