package com.stooges.controller;

import com.stooges.domain.Person;
import com.stooges.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 张超
 * @date 2017/5/9
 */
@RestController
public class RestDemoController {
    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/persons/{id}",method = RequestMethod.GET)
    public Person getPerson(@PathVariable("id") Long id){
        return personService.getPersonById(id);
    }
}
