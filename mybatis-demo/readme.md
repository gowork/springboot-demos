mybatis-spring-boot-starter使用接口或者使用`SqlSession`
- 自己编写接口
  
   使用`注解`及`xml配置`两种方式来编写sql
    
    + 创建接口，使用注解编写sql
      
      sql直接使用注解写在方法上
      ```java
        @Mapper
        public interface PersonMapper {
            @Select("select id,name,age from t_person where id=#{id}")
            Person getPersonById(@Param("id") Long id);
        }
      ```
    + 使用xml编写sql
    
      1. 在接口中定义方法
      ```java
        @Mapper
        public interface PersonMapper {
            Person getPersonById(Long id);
        }
      ```
      2. 将sql写在xml中
      ```xml
      <?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
      <mapper namespace="com.stooges.mapper.PersonMapper">
      
          <select id="getPersonById" resultType="Person" parameterType="long">
              select id,name, age from t_person WHERE id=#{id};
          </select>
      </mapper>
      ```
- 使用sqlSession的默认方法
  
  调用sql的方法是sqlSession中已经编写好的模板方法，直接调用即可，只要将sql的位置写好即可
  1. 在配置文件中配置xml文件的位置
    ```properties
    mybatis.mapper-locations=classpath*:/mapper/*Mapper.xml
    ```
  2. 编写xml形式的Mapper文件

    ```xml
  <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
    <mapper namespace="com.stooges.mapper.PersonMapper">  
        <select id="getPersonById" resultType="Person" parameterType="long">
            select id,name, age from t_person WHERE id=#{id};
        </select>
    </mapper>
    ```
  3. sqlSession直接调用方法操作数据库
    ```java
    @Service
    public class PersonService {
        @Autowired
        private SqlSession sqlSession;
        public Person getPersonById(Long id){
            return sqlSession.selectOne("com.stooges.mapper.PersonMapper.getPersonById",id);
        }
    }
    ```