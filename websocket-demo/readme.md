# springboot中使用websocket

- 引入
    ```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-websocket</artifactId>
    </dependency>
    ```
- 在controller控制器的函数中注解
    ```
    # 接收消息 在方法上使用此注解相当于使用requestMapping
    @MessageMapping("/hello")
    # 方法注解发送消息
    @SendTo("/topic/greetings")
    # 也可直接使用模板发送消息
    messageTemplate.convertAndSend("/topic/greetings2", "hello world!")
    ```
    
- 编写配置文件
- 编写页面代码