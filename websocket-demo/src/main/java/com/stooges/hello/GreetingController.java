package com.stooges.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

/**
 * websocket消息处理
 * @author 张超
 * @date 2017/11/27
 */
@Controller
public class GreetingController {
    @Autowired
    private SimpMessagingTemplate messageTemplate;

    // 接收/hello路径上传的消息
    // 给订阅/topic/greetings路径的发送消息
    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Greeting("Hello, " + message.getName() + "!");
    }

    @MessageMapping("/hello2")
    public void greeting2(HelloMessage message) throws Exception {
        Thread.sleep(2000); // simulated delay
        //使用模板直接发送消息
        messageTemplate.convertAndSend("/topic/greetings2", new Greeting("hello2, "+message.getName()+"!"));
    }
}
