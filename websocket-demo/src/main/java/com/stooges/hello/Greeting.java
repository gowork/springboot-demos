package com.stooges.hello;

/**
 * 返回的内容
 * @author 张超
 * @date 2017/11/27
 */
public class Greeting {
    private String content;

    public Greeting() {
    }

    public Greeting(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
