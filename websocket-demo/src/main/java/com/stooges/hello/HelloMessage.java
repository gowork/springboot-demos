package com.stooges.hello;

/**
 * 请求的内容
 * @author 张超
 * @date 2017/11/27
 */
public class HelloMessage {
    private String name;

    public HelloMessage() {
    }

    public HelloMessage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
