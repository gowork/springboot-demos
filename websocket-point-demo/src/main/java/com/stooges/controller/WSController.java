package com.stooges.controller;

import com.stooges.domain.SkyMessage;
import com.stooges.domain.SkyResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.security.Principal;

/**
 * @author 张超
 * @date 2017/4/11
 */
@Controller
public class WSController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/welcome")
    @SendTo("/topic/getResponse")
    public SkyResponse say(SkyMessage message)throws Exception{
        Thread.sleep(3000);

        return new SkyResponse("welcome,"+message.getName()+"!");
    }

    @MessageMapping("/chat")
    public void handleChat(Principal principal,String msg){
        if(principal.getName().equals("sky")){
            messagingTemplate.convertAndSendToUser("wyf","/queue/notifications",principal.getName()+"-send:"+msg);
        }else{
            messagingTemplate.convertAndSendToUser("sky","/queue/notifications",principal.getName()+"-send:"+msg);
        }
    }
}
