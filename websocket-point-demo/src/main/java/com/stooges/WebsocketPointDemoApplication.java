package com.stooges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketPointDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebsocketPointDemoApplication.class, args);
	}
}
