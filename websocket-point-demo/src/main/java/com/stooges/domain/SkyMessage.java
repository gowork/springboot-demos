package com.stooges.domain;

/**
 * @author 张超
 * @date 2017/4/11
 */
public class SkyMessage {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
