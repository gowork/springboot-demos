package com.stooges.domain;

/**
 * @author 张超
 * @date 2017/4/11
 */
public class SkyResponse {
    private String responseMessage;

    public SkyResponse(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
