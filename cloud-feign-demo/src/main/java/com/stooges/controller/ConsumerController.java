package com.stooges.controller;

import com.stooges.service.ComputeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 张超
 * @date 2017/4/18
 *
 */
@RestController
public class ConsumerController {
    @Autowired
    private ComputeClient computeClient;
    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public Integer add(){
        return computeClient.add(10,20);
    }
}
