package com.stooges.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 张超
 * @date 2017/4/18
 */
@FeignClient(name = "compute-service",fallback = ComputeClient.ComputeClientHystrix.class)
public interface ComputeClient {

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    Integer add(@RequestParam(value = "a") Integer a, @RequestParam(value = "b") Integer b);

    @Component
    class ComputeClientHystrix implements ComputeClient {
        @Override
        public Integer add(Integer a, Integer b) {
            return -9999;
        }
    }

}
