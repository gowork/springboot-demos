package com.stooges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@EnableDiscoveryClient
@EnableFeignClients  //开启feign功能
@SpringBootApplication
public class CloudFeignDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudFeignDemoApplication.class, args);
	}
}
