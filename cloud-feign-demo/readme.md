> feign是一个声明式的webservice客户端,方便声明服务的定义和调用
### feign使用步骤
- 在pom.xml中加入下面的依赖
    ```xml
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-feign</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-eureka</artifactId>
        </dependency>
    </dependencies>
    ```
- 在应用主类中通过`@EnableFeignClients`注解开启feign功能
```java
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class FeignApplication {
	public static void main(String[] args) {
		SpringApplication.run(FeignApplication.class, args);
	}
}
```
- 定义服务的接口
- 调用上面定义的服务
- 在配置文件中指定eureka的服务注册中心
```properties
spring.application.name=feign-consumer
server.port=3333

eureka.client.service-url.defaultZone=http://localhost:1111/eureka/
```
### 使用断路器hystrix
- 添加被@FeignClient注解注释的接口的实现类，并使用@Component注解
- @FeignClient注解添加属性fallback，值为上面实现类的class
