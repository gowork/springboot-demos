package com.stooges.consulclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 张超
 * @date 2017/12/1
 */
@RestController
public class ConsulController {
    @Autowired
    DiscoveryClient discoveryClient;

    @GetMapping("/dc2")
    public String hello(){
        String services = "services:"+discoveryClient.getServices();
        System.out.println(services);
        return services;
    }
}
