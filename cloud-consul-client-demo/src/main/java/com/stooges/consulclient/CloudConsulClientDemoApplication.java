package com.stooges.consulclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class CloudConsulClientDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudConsulClientDemoApplication.class, args);
	}
}
