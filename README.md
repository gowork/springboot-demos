### springboot 学习例子集 springboot-demos
学习springboot过程中，每个知识点作为一个demo组合成整个springboot的demo集合，每个demo中记录相关的知识点

### 功能特性
- 异步函数`@Async`
- 日志`log4j2`
- 定时任务 `@Scheduled`

### 环境依赖
- `jdk1.8`
- `springboot-start`

### 部署步骤
- clone到本地

 ```
 git clone https://git.oschina.net/gowork/springboot-demos.git
 ```
- 选择相应的功能demo直接执行主函数
> 注：需要用到maven

### 目录结构描述
- async-demo[同步说明](http://git.oschina.net/gowork/springboot-demos/tree/master/async-demo)
- log4j2-demo[介绍日志相关的内容](http://git.oschina.net/gowork/springboot-demos/tree/master/log4j2-demo)
- schedule-demo[定时任务](http://git.oschina.net/gowork/springboot-demos/tree/master/schedule-demo)
- mail-demo[邮件系统](http://git.oschina.net/gowork/springboot-demos/tree/master/mail-demo) 
- aop-demo[aop 部分](http://git.oschina.net/gowork/springboot-demos/tree/master/aop-demo)
- thymeleaf-demo[模板部分](http://git.oschina.net/gowork/springboot-demos/tree/master/thymeleaf-demo)
- websocket-demo[websocket](http://git.oschina.net/gowork/springboot-demos/tree/master/websocket-demo)
- websocket-point-demo[点对点的websocket](http://git.oschina.net/gowork/springboot-demos/tree/master/websocket-point-demo)
- cloud-eureka-demo[服务注册中心](http://git.oschina.net/gowork/springboot-demos/tree/master/cloud-eureka-demo)
- cloud-eureka-discovery-demo[服务发现客户端](http://git.oschina.net/gowork/springboot-demos/tree/master/cloud-eureka-discovery-demo)
- cloud-feign-demo[Web Service客户端](http://git.oschina.net/gowork/springboot-demos/tree/master/cloud-feign-demo)
- cloud-ribbon-demo[负载均衡器](http://git.oschina.net/gowork/springboot-demos/tree/master/cloud-ribbon-demo)
- cloud-zuul-demo[服务网关zuul](http://git.oschina.net/gowork/springboot-demos/tree/master/cloud-zuul-demo)
- cloud-config-server-demo[配置中心](http://git.oschina.net/gowork/springboot-demos/tree/master/cloud-config-server-demo)
- cloud-config-client-demo[配置访问客户端](http://git.oschina.net/gowork/springboot-demos/tree/master/cloud-config-client-demo)

### 基础部分
- @SpringBootApplication代替`@Configuration`、`@EnableAutoConfiguration`、`@ComponentScan`



