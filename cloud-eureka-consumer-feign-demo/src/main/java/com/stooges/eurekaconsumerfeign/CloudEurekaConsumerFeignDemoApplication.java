package com.stooges.eurekaconsumerfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class CloudEurekaConsumerFeignDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudEurekaConsumerFeignDemoApplication.class, args);
	}
}
