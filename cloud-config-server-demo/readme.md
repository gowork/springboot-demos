### config-server配置中心使用步骤
> 默认使用git地址存放配置文件，并且可以用版本控制，所以将文件保存在git服务器上好访问

#### 1. 创建配置仓库 ####
- 在git服务器上保存配置文件，配置文件的名称是按照下面的格式保存
    - {application}-{profile}.yml
    - {application}-{profile}.properties
- 访问地址,下面的url均会映射到默认分支master或label分支下的{application}-{profile}.properties文件
    - /{application}/{profile}[/{profile}]
    - /{application}-{profile}.yml
    - /{application}-{profile}.properties
    - /{label}/{application}-{profile}.yml
    - /{label}/{application}-{profile}.properties

#### 2. 构建config server ####
- `pom.xml`引入`spring-cloud-config-server`依赖
    ```xml
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-config-server</artifactId>
    </dependency>
    ```
- 在springboot程序主类上添加@EnableConfigServer注解开启config server
- `application.properties`中配置服务信息及git信息
    
    ```
    spring.application.name=config-server
    server.port=8001
    spring.cloud.config.server.git.url=http://git.oschina.net
    spring.cloud.config.server.git.search-paths=XX/xx
    spring.cloud.config.server.git.username=XXX@gmail.com
    spring.cloud.config.server.git.password=password
    ```
    
    > 注：可以使用spring.profile.active=native属性设置配置文件在sources目录下访问，
    也可以使用`spring.cloud.config.server.native.searchlocations=file:F:/config-repo`属性来指定配置文件的地址
