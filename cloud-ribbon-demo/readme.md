> ribbon是基于http与tcp客户端的负载均衡器，ribbon可以通过客户端配置的ribbonServerList服务端列表去轮询访问以达到均衡负载的作用

### 使用ribbon步骤
- 启动eureka服务注册中心
- 启动服务提供方
- 创建服务消费者
    - 在pom.xml中加入ribbon支持 
        ```xml
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-ribbon</artifactId>
            </dependency>
        ```
    - 在应用主类中加入`@EnableDiscoveryClient`注解来添加服务发现能力
    - 在主类中创建`RestTemplate`实例，并通过@LoadBalanced注解开启负载均衡能力
        ```java
        @EnableDiscoveryClient
        @SpringBootApplication
        public class Application{
            @Bean
            @LoadBalanced
            public RestTemplate restTemplate() {
                return new RestTemplate();
            }
        }
        ```
    - 创建消费服务的类，并通过RestTemplate来调用服务
        ```java
        @RestController  
        public class ConsumerController{
            @Autowired
            RestTemplate restTemplate;  
            @RequestMapping(value="/add",method=RequestMethod.GET)
            public String add(){
              return restTemplate.getForEntity("http://compute-service/add?a=1&b=3",String.class).getBody();
          }
        }
    
        ```
    - 在application.properties中配置eureka服务注册中心
        ```properties
        spring.application.name=ribbon-consumer
        server.port=3333
        
        eureka.client.server-url.defaultZone=http://localhost:1111/eureka/
        ```