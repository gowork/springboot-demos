package com.stooges.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author 张超
 * @date 2017/4/18
 */
@RestController
public class ConsumerController {
    @Autowired
    public RestTemplate restTemplate;
    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public String add(){
        return restTemplate.getForEntity("http://compute-service/add?a=1&b=2",String.class).getBody();
    }
}
