## log4j2
> springboot默认使用logback作为日志输出，但是据各方查证log4j2在日志性能上要绝对优于log4j及logback
遂使用log4j2作为日志输出

### 输出日志格式
- %d{HH:mm:ss.SSS} 表示输出到毫秒的时间
- %t 输出当前线程名称
- %-5level 输出日志级别，-5表示左对齐并且固定输出5个字符，如果不足在右边补0
- %logger 输出logger名称，因为Root Logger没有名称，所以没有输出
- %msg 日志文本
- %n 换行
- %F 输出所在的类文件名，如Client.java
- %L 输出行号
- %M 输出所在方法名
- %l  输出语句所在的行数, 包括类名、方法名、文件名、行数

### 日志输出级别
日志级别从低到高分为TRACE < DEBUG < INFO < WARN < ERROR < FATAL


### 使用说明
- pom.xml中排除默认，添加log4j2
  ```xml
  <dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter</artifactId>
        <exclusions>
            <exclusion>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-logging</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-log4j2</artifactId>
    </dependency>
  </dependencies>
  ```
  
- 使用配置类RollingRandomAccessFile