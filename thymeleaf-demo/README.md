## thymeleaf 模板
服务器渲染的模板，使用springboot的时候使用该模板会更加顺利，最好不要用jsp，springboot对jsp支持不是很好

## 使用说明
- 在`pom.xml`中引入模板支持
- 将模板编写在`resources`的`template`目录下

## 语法说明
- html页面转换为动态视图
    1. `<html xmlns:th="http://www.thymeleaf.org">`添加命名空间的方式
    2. `<!doctype html>`
- 属性
    1. `th:*=" "`
    2. `data-th-*=" "`
- 引用静态资源`@{}`
- 访问model中的数据`${}`
- 数据判断
    - 集合是否为空`th:if="${not #lists.isEmpty(people)}"
- 数据迭代`th:each="person:${people}"`
- javascript访问model中数据
    - 将th:inline="javascript"添加到javascript标签上
    - 通过`[[${}]]`格式获取model中的值
## 注意事项
- 为了更好地支持`html5`最好设置以下参数
    - properties配置文件需要添加下面的配置
    ```properties
      #设置模板不缓存，就是修改后不用重启
      spring.thymeleaf.cache=false
      #设置模板为html5的弱检查，默认值为html5是强检查，需要在每个标签后加结束标签
      spring.thymeleaf.mode=LEGACYHTML5
    ```
    - pom.xml需要添加下面的依赖(因为设置了非严格模式)
    ```xml
      <dependency>
          <groupId>net.sourceforge.nekohtml</groupId>
          <artifactId>nekohtml</artifactId>
          <version>1.9.22</version>
      </dependency>    
    ```
    