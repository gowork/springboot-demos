package com.stooges;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailDemoApplicationTests {

	@Autowired
	private JavaMailSender mailSender;

	@Test
	public void sendSimpleMail() throws Exception {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("发件人邮箱");
		message.setTo("收件人邮箱");
		message.setSubject("测试主题");
		message.setText("测试内容");

		mailSender.send(message);
	}

	/**
	 * 发送包含静态资源的邮件
	 * @throws Exception
	 */
	@Test
	public void sendInlineMail()throws Exception{
		MimeMessage mimeMessage = mailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
		helper.setFrom("发件人邮箱");
		helper.setTo("收件人邮箱");
		helper.setSubject("有静态文件的邮件");
		helper.setText("<html><body><img src=\"cid:weixin\"></body></html>");

		FileSystemResource file = new FileSystemResource(new File(""));
		helper.addInline("weixin",file);

		mailSender.send(mimeMessage);
	}

	/**
	 * 发送包含附件的邮件
	 * @throws Exception
	 */
	@Test
	public void sendAttachmentsMail()throws Exception{
		MimeMessage mimeMessage = mailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
		helper.setFrom("发件人的邮箱");
		helper.setTo("收件人邮箱");
		helper.setSubject("包含附件的邮件的主题");
		helper.setText("有附件");

		FileSystemResource fileSystemResource = new FileSystemResource(new File(""));
		helper.addAttachment("附件文件01.jpg",fileSystemResource);
		helper.addAttachment("附件文件02.jpg",fileSystemResource);

		mailSender.send(mimeMessage);
	}

	public void sendTemplateMail()throws Exception{
		MimeMessage mimeMessage = mailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
		helper.setFrom("发件人邮箱");
		helper.setTo("收件人邮箱");

		Map<String,String> model = new HashMap<>();
//		Template template = new FreeMarkerConfigur( "template.ftl", "UTF-8" );
//		String text = FreeMarkerTemplateUtils.processTemplateIntoString();
//		helper.setText(text,true);


	}
}
